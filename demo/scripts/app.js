(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name FuncefDemo
    * @version 1.0.0
    * @Componente para teste de componentes
    */
    angular.module('chat-demo.controller', []);

    angular
        .module('chat-demo', [
            'chat-demo.controller',
            'ui.router'
        ]);
})();
