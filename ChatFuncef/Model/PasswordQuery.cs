﻿namespace ChatFUNCEF.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class PasswordQuery
    {
        [Required]
        [Range(1, 8)]
        public string senhaAntiga { get; set; }

        [Required]
        [Range(1, 8)]
        public string senhaNova { get; set; }

        [Required]
        [Range(1, 8)]
        public string senhaConfirme { get; set; }

        public static PasswordQuery Parse(dynamic obj)
        {
            try
            {
                return new PasswordQuery
                {
                    senhaAntiga = obj.senhaAntiga.ToString(),
                    senhaNova = obj.senhaNova.ToString(),
                    senhaConfirme = obj.senhaConfirme.ToString(),
                };
            }
            catch (Exception e)
            {
                throw new InvalidCastException("Informações incorretas");
            }
        }
    }
}