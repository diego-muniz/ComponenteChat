﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;

namespace ChatFUNCEF.Messages
{
    public class ResourceGenericDialogLuis
    {
        public static string getMessage(string value)
        {
            ResourceManager rm = new ResourceManager(typeof(ResourceDialogLuis));
            return (rm.GetString(value) != null) ? rm.GetString(value).ToString() : "";
        }
    }

    public class ResourceGenericDialog
    {
        public static string getMessage(string value)
        {
            ResourceManager rm = new ResourceManager(typeof(ResourceDialog));
            return (rm.GetString(value) != null) ? rm.GetString(value).ToString() : "";
        }
    }
}