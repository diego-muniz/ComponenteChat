﻿namespace ChatFUNCEF.Dialogs
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Builder.Luis;
    using Microsoft.Bot.Builder.Luis.Models;
    using ChatFUNCEF.Log;
    using Messages;


    [LuisModel("f513bea6-40bb-4b8b-a81f-8d8f4bc5a8b7", "817cec9009434c19aee3165420ee7519")]

    [Serializable]
    public class LuisDialog : LuisDialog<object>
    {
        static LogHelper loghelper = new LogHelper(AppDomain.CurrentDomain.BaseDirectory + @"/log/Log.txt");

        /// <summary>
        /// LUIS Logic
        /// </summary>

        [LuisIntent("")]
        [LuisIntent("None")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            var message = ResourceDialog.MSG013;
            var messageLuis = ResourceGenericDialogLuis.getMessage( result.Intents[0].Intent );
            
            if ( string.IsNullOrEmpty(messageLuis) )
            {
                // No intent found, then try asking QnA Knowlegebase
                Dialogs.QnADialog qna = new Dialogs.QnADialog();
                if ( !qna.TryQuery(result.Query, out message) )
                {
                    message = $"Me Deculpe, não consegui encontrar '{result.Query}', tente detalhar mais a sua pergunta.";
                }
            }
            else
            {
                message = messageLuis;
            }

            await context.PostAsync(message);
        }

        [LuisIntent("i-saudacao-bom-dia")]
        [LuisIntent("i-saudacao-boa-noite")]
        public async Task BomDia(IDialogContext context, LuisResult result)
        {
            string message = $"resposta: '{result.Intents[0].Intent}'";

            // No intent found, then try asking QnA Knowlegebase
            //Dialogs.QnADialog qna = new Dialogs.QnADialog();
            //if (!qna.TryQuery(result.Query, out message))
            //    message = $"Sorry, I do not know'{result.Query}'";
            await context.PostAsync(message);
            //context.Done(1);    // Go back to Root
        }

    }
}