﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatFUNCEF.Dialogs
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Connector;

    [Serializable]
    public class TicketDialog : IDialog<int>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(this.MessageReceivedAsync);
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var ticketNumber = await result;

            await context.PostAsync($"Obrigado por entrar em contato com nossa equipe. O número do seu registro é {ticketNumber}.");
            await new RootDialog().StartAsync(context);
        }
    }
}