﻿namespace ChatFUNCEF.Dialogs
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Connector;
    using Repository;

    [Serializable]
    public class PasswordDialog : IDialog<bool>
    {
        private const string phoneRegexPattern = @"^(\+\d{1,2}\s)?\(?\d{2}\)?[\s.-]?\d{5}[\s.-]?\d{4}$";
        private const string emailRegexPattern = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        private const string confirmacaoSim = "Sim, estou de acordo.";
        private const string confirmacaoNao = "Não, quero retornar.";

        public async Task StartAsync(IDialogContext context)
        {
            PromptDialog.Choice(
                context,
                this.AfterChoiceSelected,
                new[] { confirmacaoSim, confirmacaoNao },
                "Por segurança, vou fazer algumas validações antes de recuperarmos sua senha. Podemos continuar?",
                "Desculpe, mas não entendi isso. Preciso que você selecione uma das opções abaixo",
                attempts: 2);
        }

        private async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;

                switch (selection)
                {
                    case confirmacaoSim:
                        await this.cpfDialog(context, result);
                        break;

                    case confirmacaoNao:
                    default:
                        await new RootDialog().StartAsync(context);
                        break;

                }
            }
            catch (TooManyAttemptsException)
            {
                await new RootDialog().StartAsync(context);
            }
        }

        private async Task cpfDialog(IDialogContext context, IAwaitable<string> result)
        {
            var cpf = await result;

            var promptCpflDialog = new PromptStringRegex(
                "Por favor, digite seu CPF:",
                CpfCnpjValidate.CpfRegexPattern,
                "O CPF informado não é válido, tente novamente:",
                "Você tentou informar seu CPF muitas vezes. Tente novamente mais tarde.",
                attempts: 2);

            context.Call(promptCpflDialog, this.dateDialog);
        }

        private async Task dateDialog(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var phone = await result;

                if (phone != null)
                {
                    await context.PostAsync($"O telefone que você forneceu é: {phone}");

                    var promptBirthDialog = new PromptDate(
                            "Estamos quase lá!!! Informe sua data de nascimento (dd/mm/aaaa):",
                            "A informação digitada não é uma data válida. Tente novamente:",
                            "Você tentou inserir sua data de nascimento muitas vezes. Tente novamente mais tarde.",
                        attempts: 2);

                    context.Call(promptBirthDialog, this.BirthDialog);
                }
                else
                {
                    context.Done(false);
                }
            }
            catch (TooManyAttemptsException)
            {
                context.Done(false);
            }
        }

        private async Task BirthDialog(IDialogContext context, IAwaitable<DateTime> result)
        {
            try
            {
                var dateOfBirth = await result;

                if (dateOfBirth != DateTime.MinValue)
                {
                    await context.PostAsync($"A data de nascimento que você forneceu é: {dateOfBirth.ToShortDateString()}");

                    var promptEmailDialog = new PromptStringRegex(
                        "Informe seu e-mail:",
                        emailRegexPattern,
                        "O e-mail informado não é válido, tente novamente:",
                        "Você tentou informar seu e-mail muitas vezes. Tente novamente mais tarde.",
                        attempts: 2);

                    context.Call(promptEmailDialog, this.phoneDialog);
                }
                else
                {
                    context.Done(false);
                }
            }
            catch (TooManyAttemptsException)
            {
                context.Done(false);
            }
        }

        private async Task phoneDialog(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var email = await result;

                if (email != null)
                {
                    await context.PostAsync($"O e-mail que você forneceu é: {email}");

                    var promptPhoneDialog = new PromptStringRegex(
                        "Digite seu número de telefone:",
                        phoneRegexPattern,
                        "O telefone inserido não é o número de telefone. Tente novamente usando o seguinte formato (99) 99999-9999:",
                        "Você tentou inserir seu número de telefone muitas vezes. Tente novamente mais tarde.",
                        attempts: 2);

                    context.Call(promptPhoneDialog, this.generatePassword);
                }
                else
                {
                    context.Done(false);
                }
            }
            catch (TooManyAttemptsException)
            {
                context.Done(false);
            }
        }

        private async Task generatePassword(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var value = await result;

                if (value != null)
                {
                    await context.PostAsync($"Sua nova senha foi encaminhado para o seu e-mail.");
                    await new RootDialog().ResumeAfterSupportDialog(context, result);
                }
                else
                {
                    context.Done(false);
                }
            }
            catch (TooManyAttemptsException)
            {
                context.Done(false);
            }
        }
    }
}
