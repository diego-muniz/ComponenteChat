﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatFUNCEF.Dialogs
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Connector;

    [Serializable]
    public class SupportDialog : IDialog<int>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(this.MessageReceivedAsync);
        }

        public static Boolean ProxySuport(string message)
        {
            if (message != null &&
                (message.ToLower().Contains("Ajuda") || message.ToLower().Contains("help") ||
                 message.ToLower().Contains("suporte") || message.ToLower().Contains("support") ||
                 message.ToLower().Contains("problema") || message.ToLower().Contains("problem")))
            {
                return true;
            }
            return false;
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var ticketNumber = new Random().Next(0, 20000);
            await context.PostAsync($"Sua mensagem '{message.Text}' foi registrada. Uma vez resolvida, entraremos em contato.");
            context.Done(ticketNumber);
        }
    }
}