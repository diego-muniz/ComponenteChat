﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace ChatFUNCEF.Dialogs
{
    [Serializable]
    public class AuthenticationDialog
    {

        private const string ConfirmaLogofS = "Sim";
        private const string ConfirmaLogofN = "Não";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> item)
        {
            PromptDialog.Choice(context, this.OnOptionSelected, new List<string>() { ConfirmaLogofS, ConfirmaLogofN }, "Are you looking for a flight or a hotel?", "Not a valid option", 3);
        }

        private async Task OnOptionSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                string optionSelected = await result;

                switch (optionSelected)
                {
                    case ConfirmaLogofS:
                        context.Fail(new NotImplementedException("Flights Dialog is not implemented and is instead being used to show context.Fail"));
                        break;

                    case ConfirmaLogofN:
                        context.Fail(new NotImplementedException("Flights Dialog is not implemented and is instead being used to show context.Fail"));
                        break;
                }
            }
            catch (TooManyAttemptsException ex)
            {
                await context.PostAsync($"Ooops! Too many attemps :(. But don't worry, I'm handling that exception and you can try again!");

                context.Wait(this.MessageReceivedAsync);
            }
        }
    }
}