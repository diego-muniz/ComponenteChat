﻿namespace ChatFUNCEF.Dialogs
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Connector;
    using System.Collections.Generic;
    using System.Threading;
    using Messages;

    [Serializable]
    public class RootDialog : IDialog<object>
    {
        private static Random random = new Random();
        private const string trocarSenha = "Trocar Senha";
        private const string recuperarSenha = "Recuperar Senha";
        private const string outrosAtendimento = "Outros Atendimentos";

        private static string[] opcoesAtendimento = { ResourceDialog.MSG005, ResourceDialog.MSG006, ResourceDialog.MSG007, ResourceDialog.MSG008 };
        private static string[] recepcaoAtendimento = { ResourceDialog.MSG001, ResourceDialog.MSG002, ResourceDialog.MSG003, ResourceDialog.MSG004 };
        private static string[] opcoesPosAtendimento = { ResourceDialog.MSG009, ResourceDialog.MSG010, ResourceDialog.MSG011, ResourceDialog.MSG012 };

        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(this.MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            // Got an Action Submit
            dynamic value = message.Value;

            if (SupportDialog.ProxySuport(message.Text))
            {
                await context.Forward(new SupportDialog(), this.ResumeAfterSupportDialog, message, CancellationToken.None);
                context.Done(false);
            }
            else if (message.Value != null)
            {
                string submitType = value.Type.ToString();
                if (submitType.Contains("confirmarAlteracao"))
                {
                    await context.Forward(new PasswordChangeDialog(), this.ResumeAfterSupportDialog, message, CancellationToken.None);
                }
            }
            else
            {
                var welcomeMessage = context.MakeMessage();
                welcomeMessage.Text = recepcaoAtendimento[random.Next(recepcaoAtendimento.Length)];
                await context.PostAsync(welcomeMessage.Text);
                Thread.Sleep(2000);
                await this.DisplayOptionsAsync(context);
            }
        }

        public async Task DisplayOptionsAsync(IDialogContext context)
        {
            PromptDialog.Choice(
                context,
                this.AfterChoiceSelected,
                new[] { recuperarSenha, trocarSenha, outrosAtendimento },
                opcoesAtendimento[random.Next(opcoesAtendimento.Length)],
                "Desculpe, mas não entendi isso. Preciso que você selecione uma das opções abaixo",
                attempts: 2);
        }

        public async Task ResumeAfterSupportDialog(IDialogContext context, IAwaitable<int> result)
        {
            await this.ResumeAfterSupportDialog(context);
        }

        public async Task ResumeAfterSupportDialog(IDialogContext context, IAwaitable<string> result)
        {
            await this.ResumeAfterSupportDialog(context);
        }

        private async Task ResumeAfterSupportDialog(IDialogContext context)
        {
            //var ticketNumber = await result;
            var welcomeMessage = context.MakeMessage();
            welcomeMessage.Text = opcoesPosAtendimento[random.Next(opcoesPosAtendimento.Length)];
            await context.PostAsync(welcomeMessage.Text);
            await this.DisplayOptionsAsync(context);
        }

        private async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;

                switch (selection)
                {
                    case recuperarSenha:
                        await new PasswordDialog().StartAsync(context);
                        break;

                    case trocarSenha:
                        await new PasswordChangeDialog().StartAsync(context);
                        break;

                    case outrosAtendimento:
                        var message = context.Activity as IMessageActivity;
                        message.Value = "";
                        await new RootTaskDialog().StartAsync(context);
                        break;

                }
            }
            catch (TooManyAttemptsException)
            {
                await this.StartAsync(context);
            }
        }

        private async Task ResumeAfterOptionDialog(IDialogContext context, IAwaitable<object> result)
        {
            context.Wait(this.MessageReceivedAsync);
        }
    }
}