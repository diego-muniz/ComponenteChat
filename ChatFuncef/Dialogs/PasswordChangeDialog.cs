﻿namespace ChatFUNCEF.Dialogs
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Bot.Builder.Dialogs;
    using System.Collections.Generic;
    using Microsoft.Bot.Connector;
    using System.Threading;
    using global::AdaptiveCards;
    using Model;
    using System.ComponentModel.DataAnnotations;
    using Repository;

    [Serializable]
    public class PasswordChangeDialog : IDialog<int>
    {
        private const string confirmacaoSim = "Sim, estou de acordo.";
        private const string confirmacaoNao = "Não, quero retornar.";
        private const string confirmarAlteracao = "Confirmar Alteração"; 

        public async Task StartAsync(IDialogContext context)
        {
            var message = context.Activity as IMessageActivity;
            // Got an Action Submit
            dynamic value = message.Value;
            if (value != null && value.Type.ToString().Contains("confirmarAlteracao"))
            {
                await this.MessageReceivedAsync(context);
            }
            else
            {
                PromptDialog.Choice(
                            context,
                            this.AfterChoiceSelected,
                            new[] { confirmacaoSim, confirmacaoNao },
                            "Por segurança, vou fazer algumas validações antes de alterarmos sua senha. Podemos continuar?",
                            "Desculpe, mas não entendi. Preciso que você selecione uma das opções abaixo",
                            attempts: 2);
            }
        }
  
        private async Task MessageReceivedAsync(IDialogContext context)
        {
            var message = context.Activity as IMessageActivity;

            if (message.Value != null)
            {
                // Got an Action Submit
                dynamic value = message.Value;
                string submitType = value.Type.ToString();
                switch (submitType)
                {
                    case "confirmarAlteracao":
                        PasswordQuery query;
                        try
                        {
                            query = PasswordQuery.Parse(value);

                            // Trigger validation using Data Annotations attributes from the PasswordQuery model
                            List<ValidationResult> results = new List<ValidationResult>();
                            bool valid = Validator.TryValidateObject(query, new ValidationContext(query, null, null), results, true);
                            if (!valid)
                            {
                                // Some field in the Password Query are not valid
                                var errors = string.Join("\n", results.Select(o => " - " + o.ErrorMessage));
                                await context.PostAsync("Por favor, complete os dados corretamente:\n" + errors);
                                return;
                            }
                            else
                            {
                                Repository.Password.Alterar();
                                message.Value = "";
                                await context.PostAsync("Senha alterada com sucesso!!!");
                                return;
                            }
                        }
                        catch (InvalidCastException e)
                        {
                            await context.PostAsync(e.Message);
                            return;
                        }
                    default:
                        await new RootDialog().StartAsync(context);
                        break;

                }
            }
        }

        private async Task ResumeAfterOptionDialog(IDialogContext context, IAwaitable<object> result)
        {
            await new RootDialog().StartAsync(context);
        }

        private async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;

                switch (selection)
                {
                    case confirmacaoSim:
                        await this.cpfDialog(context, result);
                        break;

                    case confirmacaoNao:
                        await this.StartAsync(context);
                        break;

                    default:
                        await new RootDialog().StartAsync(context);
                        break;

                }
            }
            catch (TooManyAttemptsException)
            {
                await new RootDialog().StartAsync(context);
            }
        }

        private async Task cpfDialog(IDialogContext context, IAwaitable<string> result)
        {
            var cpf = await result;

            var promptCpflDialog = new PromptStringRegex(
                "Por favor, digite seu CPF:",
                CpfCnpjValidate.CpfRegexPattern,
                "O CPF informado não é válido, tente novamente:",
                "Você tentou informar seu CPF muitas vezes. Tente novamente mais tarde.",
                attempts: 2);

            context.Call(promptCpflDialog, this.dateDialog);
        }

        private async Task dateDialog(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var cpf = await result;

                if (cpf != null)
                {
                    await context.PostAsync($"O CPF que você forneceu é: {cpf}");

                    var promptBirthDialog = new PromptDate(
                            "Falta pouco, informe sua data de nascimento (dd/mm/aaaa):",
                            "O data que você digitou não é uma data válida, informe o padrão (dd/mm/aaaa):",
                            "Você tentou inserir sua data de nascimento muitas vezes. Tente novamente mais tarde.",
                        attempts: 2);

                    context.Call(promptBirthDialog, this.BirthDialog);
                }
                else
                {
                    context.Done(false);
                }
            }
            catch (TooManyAttemptsException)
            {
                context.Done(false);
            }
        }
        
        private async Task BirthDialog(IDialogContext context, IAwaitable<DateTime> result)
        {
            try
            {
                var dateOfBirth = await result;

                if (dateOfBirth != DateTime.MinValue)
                {
                    await context.PostAsync($"A data de nascimento que você forneceu é: {dateOfBirth.ToShortDateString()}");
                    await this.ShowFormOptionsAsync(context);
                }
                else
                {
                    context.Done(false);
                }
            }
            catch (TooManyAttemptsException)
            {
                context.Done(false);
            }
        }

        private async Task ShowFormOptionsAsync(IDialogContext context)
        {
            Attachment attachment = new Attachment()
            {
                ContentType = AdaptiveCard.ContentType,
                Content = GetContentCard()
            };

            var reply = context.MakeMessage();
            reply.Attachments.Add(attachment);

            await context.PostAsync(reply, CancellationToken.None);
        }
        
        private static AdaptiveCard GetContentCard()
        {
            return new AdaptiveCard()
            {
                Body = new List<CardElement>()
                {
                    new TextBlock()
                    {
                        Text = "Trocar Minha Senha",
                        Speak = "<s>Trocar Minha Senha!</s>",
                        Weight = TextWeight.Bolder,
                        Size = TextSize.Large
                    },
                    new TextBlock() { Text = "Regras para geração da nova senha: mínimo 8 caracteres, letras maiúscula e minúscula, caractere especial." },
                    new TextBlock() { Text = "Informe sua senha antiga *:" },
                    new TextInput() 
                    {
                        Id = "senhaAntiga",
                        Speak = "<s>Senha Antiga</s>",
                        Style = TextInputStyle.Text
                    },
                    new TextBlock() { Text = "Informe sua nova senha *:" },
                    new TextInput()
                    {
                        Id = "senhaNova",
                        Speak = "<s>Senha Nova</s>",
                        Style = TextInputStyle.Text
                    },
                    new TextBlock() { Text = "Confirme sua nova senha *:" },
                    new TextInput()
                    {
                        Id = "senhaConfirme",
                        Speak = "<s>Confirme sua nova senha</s>",
                        Style = TextInputStyle.Text
                    }
                },
                Actions = new List<ActionBase>()
                {
                    new SubmitAction()
                    {
                        Title = $"{confirmarAlteracao}",
                        Speak = $"<s>{confirmarAlteracao}</s>",
                        DataJson = "{ \"Type\": \"confirmarAlteracao\" }"
                    }
                }
            };
        }
    }
}