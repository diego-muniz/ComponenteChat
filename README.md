# Componente - chat-bot

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Uso](#uso)
6. [Desinstalação](#desinstalação)


## Descrição

- Componente para utilização de bot.

 
## Instalação:

- Navegue até a pasta ComponenteChat via prompt e execute os comandos a seguir.

### Npm

- Necessário executar o npm install.

```
npm install
```

### Bower

- Necessário executar o bower install.

```
bower install
```

## Script

```html
<script src="bower_components/chat-bot/dist/chat-bot.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/chat-bot/dist/chat-bot.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep


## Uso

```html

<div id="bot">
	<script type="text/javascript" >
		FuncefBot.app({
			directLine: { secret: 'YOUR KEY DIRECTLINE'},
			user: { id: 'YOUR KEY'},
			bot: { id: 'YOUR KEY BOT'},
			resize: 'detect'
		}, 'bot', 'Titulo');		
	</script>
</div>

```

## Desinstalação:

```
npm uninstall
bower uninstall
```