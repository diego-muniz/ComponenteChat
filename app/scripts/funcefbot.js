var FuncefBot = {
	id : null,
	obj : null,
	title : null,
	element: null,

	app: function(obj, id, title) {
		BotChat.App(obj, document.getElementById(id));
		this.obj = obj;
		this.id = id;
		this.title = title;
		this.init();
	},

	init: function() {
		this.element = $('#' + this.id);
		this.layout();
	},

	layout: function() {
		this.element.addClass('funcef-bot');
		this.addButtonHideShow();
		this.addEventHideShow();
		this.addButtonClose();
		this.changeTitle();
	},

	changeTitle: function() {
		$('.wc-header span', FuncefBot.element).text(this.title ? this.title : 'Chat');
	},

	addEventHideShow: function () {
		var abrirChat = $('.abrirChat')

		abrirChat.on('click', function(){
			FuncefBot.element.fadeIn(300);
			$(this).hide();
		});

		$('.wc-header', FuncefBot.element).on('click', function(){
			FuncefBot.element.fadeOut(150);
			abrirChat.show();
		});
		
	},

	addButtonClose: function() {
		$('.wc-header', FuncefBot.element).append('<i class="fa fa-times"></span>');
	},

	addButtonHideShow: function() {
		var $body = $('body');
		$('.abrirChat', $body).remove();
		$body.append('<div class="abrirChat">'
							+ '<div>'
						        + '<i class="fa fa-comments-o" aria-hidden="true"></i>'
						    + '</div>'
						    + '<div>'
						        + '<span>Precisando de ajuda?</span>'
						    + '</div>'
						+ '</div>');
	}
};
